import os
import pandas as pd
from tqdm import tqdm
from mpi4py import MPI
import bigmpi4py as bg


comm = MPI.COMM_WORLD
rank = comm.rank
size = comm.size





dir_beds = '/media/SETH_DATA/SETH_Alex/WGBS-ENCODE/BED/'

metadata = pd.read_csv(dir_beds + 'metadata.tsv', sep='\t')

metadata_CpG = metadata[metadata['Output type'] == 'methylation state at CpG']

names = metadata_CpG['File accession'].values.tolist()
list_chr = []

if rank != 0:
    names = None

names = bg.scatter(names, comm)


comm.Barrier()

for i in tqdm(names):
    os.system('gunzip ' + dir_beds + i + '.bed.gz')

