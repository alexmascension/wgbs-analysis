from mpi4py import MPI
import bigmpi4py as BP
import numpy as np
import pandas as pd
from scipy.stats import kruskal
from tqdm import tqdm
import time
import argparse

comm = MPI.COMM_WORLD


def runKW(h5, metadata, pval_table):
    metadata_df = pd.read_csv(metadata, sep='\t')

    if comm.rank == 0:
        # Load h5 file, and sort the columns to match `accession_list`.
        WGBS_df = pd.read_hdf(h5, key='table')

        # For the sake of simplicity, we will remove the placenta from the samples
        accession_list = metadata_df['File accession'][
            ~metadata_df['Biosample term name'].isin(['placenta', 'spinal cord'])].values

        WGBS_df = WGBS_df[['chr', 'pos'] + [i for i in accession_list if i in WGBS_df.columns.values]]

        accession_endo = [i for i in metadata_df['File accession'][metadata_df['Germ layer'] == 'endoderm'].values
                          if i in WGBS_df.columns.values]
        accession_meso = [i for i in metadata_df['File accession'][metadata_df['Germ layer'] == 'mesoderm'].values
                          if i in WGBS_df.columns.values]
        accession_ecto = [i for i in metadata_df['File accession'][metadata_df['Germ layer'] == 'ectoderm'].values
                          if i in WGBS_df.columns.values]

        WGBS_endo = WGBS_df[accession_endo].values
        WGBS_meso = WGBS_df[accession_meso].values
        WGBS_ecto = WGBS_df[accession_ecto].values

    else:
        WGBS_endo, WGBS_meso, WGBS_ecto = None, None, None


    WGBS_endo = BP.scatter(WGBS_endo, comm)
    WGBS_meso = BP.scatter(WGBS_meso, comm)
    WGBS_ecto = BP.scatter(WGBS_ecto, comm)

    assert len(WGBS_ecto) == len(WGBS_endo)
    assert len(WGBS_endo) == len(WGBS_meso)

    list_pvals = []

    if comm.rank == 0:
        t = time.time()

    for i in tqdm(range(len(WGBS_endo)), position=0, desc='CPU: %s'%comm.rank):
        try:
            endo_array = WGBS_endo[i,:]
            meso_array = WGBS_meso[i,:]
            ecto_array = WGBS_ecto[i,:]

            list_pvals.append(kruskal(endo_array, meso_array, ecto_array)[1])
        except ValueError:
            list_pvals.append(1)

    if comm.rank == 0:
        print('Computation time: ' + str(time.time() - t))

    list_pvals = np.array(list_pvals)
    list_pvals = BP.gather(list_pvals, comm=comm)

    if comm.rank == 0:
        np.savetxt(pval_table, list_pvals,)


parser = argparse.ArgumentParser(description='Process parallel Kruskal Wallis test.')
parser.add_argument('--h5')
parser.add_argument('--metadata')
parser.add_argument('--pval_table')


args = parser.parse_args()


runKW(args.h5, args.metadata, args.pval_table)




