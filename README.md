# WGBS analysis

This repository contains the python files and jupyter notebooks used to do the biological analysis using BigMPI4py.

The file distribution is as follows:

In ``BED`` file there is ``metadata.tsv``, which contains the accesion names to
download all the datasets. All files qill be downloaded into ``BED``.

In order to download the files, run the first cells on the notebook ``WGBS analysis.ipynb``, 
which will download all bed files, and then will gunzip them. 

The file ``BED-preprocess.py`` combines all methylation information from bed files
and combines it into one HDF file: ``merge_sorted.h5``. 

The file ``parallel_KW.py`` contains all the paralle implementation of the
Kruskal-Wallis test, to run it in parallel. There is a cell on the notebook that
already runs this step.

The file ``WGBS analysis.ipynb`` is the jupyter notebook with the whole analysis
to be replicated. Since the analysis is heavy (datasets are memory-intensive) 
jupyter notebook may crash. If that is the case, you can run the analysis with
``WGBS analysis.py``.


