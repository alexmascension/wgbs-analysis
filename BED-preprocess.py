import os
import pandas as pd
from tqdm import tqdm
from mpi4py import MPI
import bigmpi4py as bg


comm = MPI.COMM_WORLD
rank = comm.rank
size = comm.size





dir_beds = os.path.dirname(os.path.abspath(__file__)) + '/BED/'

metadata = pd.read_csv(dir_beds + 'metadata.tsv', sep='\t')

metadata_CpG = metadata[metadata['Output type'] == 'methylation state at CpG']

print("Passing BEDs to names and chrm \n")
names = [i for i in metadata_CpG['File accession'].values.tolist() if os.path.exists(dir_beds+i+'.bed')]

comm.Barrier()
if rank == 0:
    for name in tqdm(names):
        file_exists = True
        list_chr = ['chr' + str(i) for i in range(1, 23)] + ['chrX', 'chrY']

        for i in list_chr:
            if not os.path.exists(dir_beds + name + '_%s.h5' % i):
                file_exists = False
                break

        if not file_exists:
            print(name)
            df = pd.read_csv(dir_beds + name + '.bed', sep='\t', header=None)
            df = df.iloc[:, [0, 1, -1]]


            for chr in list(set(df.iloc[:,0].values.tolist())):
                if len(chr) < 7:
                    print(chr)

                    df_chr = df[df.iloc[:,0] == chr]
                    df_chr = df_chr.iloc[:,1:]
                    df_chr = df_chr.sort_values(by = df.columns.values[-1])
                    df_chr.to_hdf(dir_beds + name + '_' + chr + '.h5', key='table')



names = [i for i in metadata_CpG['File accession'].values.tolist() if os.path.exists(dir_beds+i+'_chr1.h5')]


if rank == 0:
    list_chr = ['chr' + str(i) for i in range(1,23)] + ['chrX', 'chrY']
else:
    list_chr = None


list_chr = bg.scatter(list_chr, comm)

df_merge_chr = pd.DataFrame()


for chr in list_chr:
    if not os.path.exists(dir_beds + '/merged_' + chr + '.h5'):
        list_dfs = []
        for name in tqdm(names[:]):
            df_i = pd.read_hdf(dir_beds + name + '_' + chr + '.h5', key='table')
            df_i = df_i.rename(columns={df_i.columns.values[0]:'pos', df_i.columns.values[-1]: name})
            df_i  = df_i.sort_values(by='pos')
            df_i = df_i.set_index('pos')
            list_dfs.append(df_i)

        merged_df = pd.concat(list_dfs, axis=1)

        print('Merged %s -> %s' %(chr, merged_df.shape))
        merged_df.to_hdf(dir_beds + '/merged_'+chr+'.h5', key='table')

        del merged_df


comm.Barrier()

if rank == 0:
    list_chr = ['chr' + str(i) for i in range(1, 23)] + ['chrX', 'chrY']

    list_dfs = []
    names = []
    for chr in tqdm(list_chr):
        print(os.path.exists(dir_beds + '/merged_' + chr + '.h5'))
        chr_df = pd.read_hdf(dir_beds + '/merged_' + chr + '.h5', key='table')

        names += chr_df.columns.values.tolist()
        chr_df['pos'] = chr_df.index
        chr_df['chr'] = chr

        list_dfs.append(chr_df)

    print('FINAL MERGE ', len(list_dfs), len(names))
    print('concat')
    merged_df = pd.concat(list_dfs, axis=0)
    merged_df = merged_df[['chr', 'pos'] + sorted(list(set(names)))]
    merged_df = merged_df.reset_index(drop=True)
    print('sort')
    merged_sorted_df = merged_df.sort_values(by=['chr', 'pos']).reset_index(drop=True)

    merged_df.to_hdf(dir_beds + '/merge.h5', key='table')
    merged_sorted_df.to_hdf(dir_beds + '/merge_sorted.h5', key='table')
